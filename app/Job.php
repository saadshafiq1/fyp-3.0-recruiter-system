<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    public function recruiter()
    {
        return $this->belongsTo(Admin::class);
    }

    public function user()
    {
        return $this->belongsToMany(User::class, 'job_users','job_id','user_id');
    }


}
