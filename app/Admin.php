<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    protected $guard = 'admin';
    protected $table = 'admins';
    protected $redirectTo = '/';
    protected $fillable = ['first_name', 'last_name', 'sex', 'email', 'password'];

    public function jobs()
    {
        return $this->hasMany(Job::class);
    }
}
