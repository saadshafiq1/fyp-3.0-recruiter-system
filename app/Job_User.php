<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job_User extends Model
{
    protected $table = 'job_users';

    public function user()
    {
        return $this->belongsTo(User::class,'id');
    }

    public function job()
    {
        return $this->belongsTo(Job::class,'id');
    }
}
