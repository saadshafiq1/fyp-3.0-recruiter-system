<?php

namespace App\Http\Controllers;

use App\Http\Requests\ResumeRequest;
use App\Resume;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResumeController extends Controller
{
    public function create()
    {
        return view('User.resume');
    }

    public function store(ResumeRequest $request)
    {
        $resume = Auth::user();
//        $resume->user_id = Auth::user()->id;
        $resume->experience = $request->experience;
//        $resume->salary = $request->salary;
        $resume->gender = $request->gender;
        $resume->description = $request->description;
        $resume->mobile_number = $request->mobile_number;
        $resume->work_email = $request->work_email;
        $request->session()->flash('created', 'Your resume has been created');
        $resume->update();
        return redirect(route('home'));
    }

}
