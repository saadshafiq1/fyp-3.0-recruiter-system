<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
         $recruiter = Auth::user();
//         foreach($recruiter->jobs as $job){
//             foreach($job->user as $user){
//                 return $user;
//             }
//         }
        return view('recruiter.index');
    }

    public function myJobs()
    {
        $recruiter = Auth::guard("admin")->user();
//        foreach ($recruiter->jobs as $job){
//            foreach($job->user as $user){
//                return $user->resume;
//            }
//        }
        return view('Recruiter.userapplied.index', compact('recruiter'));
    }
}
