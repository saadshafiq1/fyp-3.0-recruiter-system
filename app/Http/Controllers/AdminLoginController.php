<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminLoginController extends Controller
{

    protected $redirectTo = '/admin';
    protected $guard = 'admin';


    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginForm()
    {
        if (view()->exists('auth_recruiter.login')) {
            return view('auth_recruiter.login');
        }

        return view('/');
    }


    public function login(Request $request)
    {

        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            return redirect()->intended(route('recruiter.jobs'));
        }
        $request->session()->flash('error','Your Password or Email is Invalid');
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect(route('admin.login'));
    }

    public function index()
    {
        if (view()->exists('auth_recruiter.register')) {
            return view('auth_recruiter.register');
        }
        return view('/');
    }

    public function register(Request $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make($request['password']);
        \App\Admin::create($input);
        return redirect(route("admin.dashboard"));
    }
}
