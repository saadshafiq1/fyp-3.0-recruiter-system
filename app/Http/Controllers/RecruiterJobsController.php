<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateJobRequest;
use App\Job;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RecruiterJobsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function create()
    {
        return view('Recruiter.job.create');
    }

    public function store(CreateJobRequest $request)
    {
//        return $request->all();
        $job = new Job();
        $job->job_position = $request->name;
        $job->job_description = $request->description;
        $job->salary = $request->salary;
        $job->admin_id = Auth::guard('admin')->user()->id;
        $job->save();
        $request->session()->flash('job_created','Job has been published successfully');
        return redirect(route('recruiter.jobs'));
    }

    public function update(Request $request, $id)
    {
        $job = Job::findOrFail($id);
        $job->job_position = $request->name;
        $job->description = $request->decription;
        $job->salary = $request->salary;
        $job->save();
        return back();
    }

    public function destroy($id)
    {
        $job = Job::findOrFail($id);
        $job->delete();
        return back();
    }
    public function specificUser($id)
    {
        $user=User::findOrFail($id);
//        return $user->test;
        return view('Recruiter.userapplied.specific_user',compact('user'));
    }
}
