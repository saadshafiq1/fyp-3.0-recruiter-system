<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Recruiter extends Controller
{

    protected $redirectTo = '/';
    protected $guard = 'recruiter';


    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }
    public function index()
    {
        if (view()->exists('Admin.auth.reg_form')) {
            return view('Admin.auth.reg_form');
        }
        return view('/');
    }

    public function register(Request $request)
    {
        $input = $request->all();
//        $recruiter = new Admin();
//        $recruiter->name = $request->name;
//        $recruiter->password = $request->password;
//        $recruiter->email = $request->email;
//        $recruiter->update();
        $input['password'] = bcrypt($request->passwrod);
        \App\Admin::create($input);
        return redirect()->back();
    }

    public function loginForm()
    {
        return view('auth_recruiter.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        if (Auth::guard('recruiter')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        }
        return redirect('/recruiter')->withInput($request->only('email', 'remember'));

    }

    public function logout()
    {
        Auth::guard('recruiter')->logout();
        return redirect('/');
    }

    public function home()
    {
        return view('admin.index');
    }

    public function indexHome()
    {
        return view('index');
    }
}

