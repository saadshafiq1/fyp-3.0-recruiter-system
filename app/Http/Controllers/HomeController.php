<?php

namespace App\Http\Controllers;

use App\Job;
use App\Job_User;
use App\Resume;
use App\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $jobCount = Job::count();
        $jobs = Job::all();
        return view('User.home', compact('jobs', 'user', 'jobCount'));
    }

    public function job($id)
    {
        $job = Job::findOrFail($id);
        return view('User.job', compact('job'));
    }

     public function specificJob($id)
     {
         $job = Job::findOrFail($id);
         return view("User.specific-job", compact('job'));
     }

    public function showResume( $id)
    {
        $job = Job::findOrFail($id);
        $user = Auth::user();
        return view('User.form', compact('job','user'));
    }

    public function UserJobStore(Request $request, $id)
    {
        $resume = Auth::user();
//      $resume->user_id = Auth::user()->id;
        $resume->experience = $request->experience;
//      $resume->salary = $request->salary;
        $resume->gender = $request->gender;
        $resume->description = $request->description;
        $resume->mobile_number = $request->mobile_number;
        $resume->work_email = $request->work_email;
        $resume->salary = $request->salary;
        $resume->skills = $request->skills;
        $resume->update();
        $jobuser = Job_User::where('job_id', $id)->where('user_id',$resume->id)->first();
        if (isset($jobuser)){
            $request->session()->flash('already','You have already applied for this job');
            return redirect(route('home'));
        }
        $jobUser = new Job_User();
        $jobUser->job_id = $id;
        $jobUser->user_id = Auth::user()->id;
        $jobUser->save();
        $request->session()->flash('applied','You have successfully applied for the job');
        return redirect(route("home"));
    }

    public function giveTest(Request $request)
    {
        $test= new Test();
        $test->likert1 = $request->likert1;
        $test['user_id']=Auth::user()->id;
        $test->likert2 = $request->likert2;
        $test['user_id']=Auth::user()->id;
        $test->likert3 = $request->likert3;
        $test['user_id']=Auth::user()->id;
        $test->likert4 = $request->likert4;
        $test['user_id']=Auth::user()->id;
        $test->likert5 = $request->likert5;
        $test['user_id']=Auth::user()->id;
        $test->likert6 = $request->likert6;
        $test['user_id']=Auth::user()->id;
        $test->likert7 = $request->likert7;
        $test['user_id']=Auth::user()->id;
        $test->likert8 = $request->likert8;
        $test['user_id']=Auth::user()->id;
        $test->likert9 = $request->likert9;
        $test['user_id']=Auth::user()->id10;
        $test->likert10 = $request->likert11;
        $test['user_id']=Auth::user()->id;
        $test->likert11 = $request->likert12;
        $test['user_id']=Auth::user()->id;
        $test->likert12 = $request->likert13;
        $test['user_id']=Auth::user()->id;
        $test->likert13 = $request->likert14;
        $test['user_id']=Auth::user()->id;
        $test->likert14 = $request->likert15;
        $test['user_id']=Auth::user()->id;
        $test->likert15 = $request->likert16;
        $test['user_id']=Auth::user()->id;
        $test->likert16 = $request->likert17;
        $test['user_id']=Auth::user()->id;
        $test->likert17 = $request->likert18;
        $test['user_id']=Auth::user()->id;
        $test->likert18 = $request->likert19;
        $test['user_id']=Auth::user()->id;
        $test->likert19 = $request->likert20;
        $test['user_id']=Auth::user()->id;
        $test->likert20 = $request->likert21;
        $test['user_id']=Auth::user()->id;
        $test->likert21 = $request->likert22;
        $test['user_id']=Auth::user()->id;
        $test->likert22 = $request->likert23;
        $test['user_id']=Auth::user()->id;
        $test->likert23 = $request->likert24;
        $test['user_id']=Auth::user()->id;
        $test->likert24 = $request->likert25;
        $test['user_id']=Auth::user()->id;
        $test->likert25 = $request->likert26;
        $test['user_id']=Auth::user()->id;
        $test->likert26 = $request->likert27;
        $test['user_id']=Auth::user()->id;
        $test->likert27 = $request->likert28;
        $test['user_id']=Auth::user()->id;
        $test->likert28 = $request->likert29;
        $test['user_id']=Auth::user()->id;
        $test->likert29 = $request->likert30;
        $test['user_id']=Auth::user()->id;
        $test->likert30 = $request->likert31;
        $test['user_id']=Auth::user()->id;
        $test->likert31 = $request->likert32;
        $test['user_id']=Auth::user()->id;
        $test->likert32 = $request->likert33;
        $test['user_id']=Auth::user()->id;
        $test->likert33 = $request->likert34;
        $test['user_id']=Auth::user()->id;
        $test->likert34 = $request->likert35;
        $test['user_id']=Auth::user()->id;
        $test->likert35 = $request->likert36;
        $test['user_id']=Auth::user()->id;
        $test->likert36 = $request->likert37;
        $test['user_id']=Auth::user()->id;
        $test->likert37= $request->likert38;
        $test['user_id']=Auth::user()->id;
        $test->likert38 = $request->likert39;
        $test['user_id']=Auth::user()->id;
        $test->likert39 = $request->likert40;
        $test['user_id']=Auth::user()->id;
        $test->likert40 = $request->likert41;
        $test['user_id']=Auth::user()->id;
        $test->likert41 = $request->likert42;
        $test['user_id']=Auth::user()->id;
        $test->likert42 = $request->likert43;
        $test['user_id']=Auth::user()->id;
        $test->likert43 = $request->likert44;
        $test['user_id']=Auth::user()->id;
        $test->likert44 = $request->likert45;
        $test['user_id']=Auth::user()->id;
        $test->likert45 = $request->likert46;
        $test['user_id']=Auth::user()->id;
        $test->likert46 = $request->likert47;
        $test['user_id']=Auth::user()->id;
        $test->likert47 = $request->likert48;
        $test['user_id']=Auth::user()->id;
        $test->likert48 = $request->likert49;
        $test['user_id']=Auth::user()->id;
        $test->likert49 = $request->likert50;
        $test['user_id']=Auth::user()->id;
        $test->likert50 = $request->likert51;
        $test['user_id']=Auth::user()->id;
        $test->likert51 = $request->likert52;
        $test['user_id']=Auth::user()->id;
        $test->likert52 = $request->likert53;
        $test['user_id']=Auth::user()->id;
        $test->likert53 = $request->likert54;
        $test['user_id']=Auth::user()->id;
        $test->likert54 = $request->likert55;
        $test['user_id']=Auth::user()->id;
        $test->likert55 = $request->likert56;
        $test['user_id']=Auth::user()->id;
        $test->likert56 = $request->likert57;
        $test['user_id']=Auth::user()->id;
        $test->likert57 = $request->likert58;
        $test['user_id']=Auth::user()->id;
        $test->likert58 = $request->likert59;
        $test['user_id']=Auth::user()->id;
        $test->likert59 = $request->likert59;
        $test['user_id']=Auth::user()->id;
        $test->likert60 = $request->likert60;
        $test['user_id']=Auth::user()->id;

        if($test->save()){
            $output =[];
            $return_var = "";
            exec("cd ../&& C:\Users\User\AppData\Local\Programs\Python\Python37-32\python predict.py", $output, $return_var);
            if($return_var==0) {
                $result = json_decode($output[0]);
            }
            else return "Something went wrong, Please try again later";
        }
        $request->session()->flash('applied','Personality Test submitted');
        return redirect(route("home"));
    }


    public function testform()
    {
        return view('User.test');
    }

    public function destroy($id)
    {
        $this->destroy($id);


    }
}

