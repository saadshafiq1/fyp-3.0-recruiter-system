<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resume extends Model
{
    public function resume()
    {
        return $this->belongsTo(User::class);
    }
}
