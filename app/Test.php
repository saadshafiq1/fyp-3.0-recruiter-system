<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    public function test()
    {
        return $this->belongsTo(User::class);
    }

    public function personalities(){
        return $this->hasMany(UserPersonality::class);
    }
}
