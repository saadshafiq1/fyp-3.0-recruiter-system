<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','experience'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function resume()
    {
        return $this->hasOne(Resume::class);
    }

    public function jobs()
    {
        return $this->belongsToMany(Job::class, 'job_users','user_id','job_id');
    }

    public function test()
    {
        return $this->hasOne(Test::class);
    }
//    public function getMyIdAttribute(){
//        $recent_test_id = Test::where("user_id", $this->getKey("id"))->orderBy("id", "desc")->first();
//        return (String) $recent_test_id->id;
//    }
    public function personalities(){
        return $this->hasMany(UserPersonality::class);
    }
    public function recentPersonalities(){
        $recent_test_id = Test::where("user_id", $this->getKey("id"))->orderBy("id", "desc")->first();
        return $this->personalities()->where("test_id", $recent_test_id->id);
    }
}
