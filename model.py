import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.svm import *
from sklearn.neighbors import KNeighborsClassifier
from sklearn import linear_model
from sklearn import tree
import pickle
from sklearn.externals import joblib


from sklearn.preprocessing import *

file = open("formattedanswer.csv", "r")

label_start_indx = 60
limit = 99

data_X = None
data_y = None
labels = None

isextrovert_train = None
isintuitive_train = None
thinking_train = None
isjudging_train = None
isassertive_train = None

isextrovert_test = None
isintuitive_test = None
thinking_test = None
isjudging_test = None
isassertive_test = None


def get_labels(file):
    
    # getting the first line from file as labels
    first_line = file.readline()

    # splitting labels on commas
    first_line = first_line.split(',')[label_start_indx:]
    first_line = [i.rstrip() for i in first_line]
    
    return first_line

def get_data_X_Y(file):
    X = []
    y = []
    
    data = file.readline()

    while ( data != "" ):
    # for i in range(limit): # for temp purposes

        data = data.split(',')

        X.append(data[:label_start_indx])

        y.append([i.rstrip() for i in data[label_start_indx:]])
        
        data = file.readline()

    return np.matrix(X, dtype = np.float32), np.matrix(y, dtype = np.float32)

def print_dimen(x):
    print(len(x), len(x[0]))


def print_all_data_size(X,y):
    print("Training data:", X.shape)
    print("Testing data:", y.shape)
###########################################################################

if __name__ == "__main__":

    labels = np.array(get_labels(file))
    data_X, data_y = get_data_X_Y(file)

    ############ SCALING ##################################
    
    # data_X = MinMaxScaler().fit_transform(data_X)
    # data_X = MaxAbsScaler().fit_transform(data_X)

    #######################################################

    X_train, X_test, y_train, y_test = train_test_split(data_X, data_y, test_size=0.3, shuffle = False)

    training_data_y = []
    testing_data_y = []
    for i in range(len(labels)):
        training_data_y.append(y_train[:,i])
        testing_data_y.append(y_test[:,i])

    clf = []
    for i in range(len(labels)):

        ############## SETTING CLASSIFIER ####################################################

        # using linear SVM
        # clf.append(LinearSVC(tol = 0.000001, max_iter = 9999999))

        # using Nu SVM
        clf.append(NuSVC(kernel = "rbf", degree = 3, gamma = "auto", max_iter = -1))

        # using C-Support SVM
        # clf.append(SVC(gamma = "auto", kernel = "poly", tol = 0.000001, max_iter = -1))
        # clf.append(SVC(gamma = "auto", max_iter = -1))

        # using KNeighborsClassifier 
        # clf.append(KNeighborsClassifier())

        # using Decision Tree Classifier
        # clf.append(tree.DecisionTreeClassifier())

        #####################################################################################

        clf[i].fit(X_train, training_data_y[i].A1)
        print(labels[i],"accuracy:", clf[i].score(X_test, testing_data_y[i].A1))

    filename = 'finalized_model.pkl'
    joblib.dump(clf, open(filename, 'wb'))

    """
    This file should be executed to train the model 
    once the accuracies are good no need to use this file now
    Since a usable model has been written in a file when this is one
    After training that written file has to used for prediction
    """
