<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use App\Test;

Route::get("/test123", function(){
    $output =[];
    $return_var = "";
    exec("cd ../&& C:\Users\User\AppData\Local\Programs\Python\Python37-32\python predict.py", $output, $return_var);
    if($return_var==0) {
        $result = json_decode($output[0]);
    }
    else return "Something went wrong, Please try again later";

});

Route::get("/personalities/{id}", function($id){
    $test = Test::where("user_id", $id)->orderBy("id", "desc")->with("personalities")->first();
    return $test->personalities;
});








Auth::routes();
Route::prefix('admin')->group(function (){

    Route::get('/login', 'AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'AdminLoginController@login')->name('admin.login.submit');
    Route::get('/logout', 'AdminLoginController@logout')->name('admin.logout');
    Route::get('/dashboard/admin', 'AdminController@index')->name('admin.dashboard');
    Route::get('/job/create', 'RecruiterJobsController@create')->name('admin.job.create');
    Route::get('/job/user/request', 'AdminController@myJobs')->name('recruiter.jobs');
    Route::get('/user/specific/{id}','RecruiterJobsController@specificUser')->name('specific.user');


});
Route::get('/reg/form', 'AdminLoginController@index')->name('recruiter.register');
Route::post('/reg/form/save', 'AdminLoginController@register')->name('recruiter.save');
Route::get('/home','HomeController@index')->name('home');
Route::get('/','Controller@index')->name('index');
Route::get('/cv/form', 'ResumeController@create')->name('resume.form');
Route::post('/cv/submit', 'ResumeController@store')->name('data.store');
Route::get('/recruiter/job', 'RecruiterJobsController@create')->name('add.job');
Route::post('/recruiter/job/store', 'RecruiterJobsController@store')->name('job.store');
Route::get('/specific/job/{id}','HomeController@specificJob')->name('specific.job');
Route::get('/specific/job/{id}/user','HomeController@showResume')->name('apply.now');
Route::post('/user/apply/job/{id}', 'HomeController@userJobStore')->name('user.job.store');
Route::get('/user/apply/test','HomeController@testform')->name('test.form');
Route::post('/user/five/test','HomeController@giveTest')->name('give.test');
Route::get('destroy/{id}','RecruiterJobsController@destroy')->name('destroy');




