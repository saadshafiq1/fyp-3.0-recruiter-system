<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->tinyInteger('likert1');
            $table->tinyInteger('likert2');
            $table->tinyInteger('likert3');
            $table->tinyInteger('likert4');
            $table->tinyInteger('likert5');
            $table->tinyInteger('likert6');
            $table->tinyInteger('likert7');
            $table->tinyInteger('likert8');
            $table->tinyInteger('likert9');
            $table->tinyInteger('likert10');
            $table->tinyInteger('likert11');
            $table->tinyInteger('likert12');
            $table->tinyInteger('likert13');
            $table->tinyInteger('likert14');
            $table->tinyInteger('likert15');
            $table->tinyInteger('likert16');
            $table->tinyInteger('likert17');
            $table->tinyInteger('likert18');
            $table->tinyInteger('likert19');
            $table->tinyInteger('likert20');
            $table->tinyInteger('likert21');
            $table->tinyInteger('likert22');
            $table->tinyInteger('likert23');
            $table->tinyInteger('likert24');
            $table->tinyInteger('likert25');
            $table->tinyInteger('likert26');
            $table->tinyInteger('likert27');
            $table->tinyInteger('likert28');
            $table->tinyInteger('likert29');
            $table->tinyInteger('likert30');
            $table->tinyInteger('likert31');
            $table->tinyInteger('likert32');
            $table->tinyInteger('likert33');
            $table->tinyInteger('likert34');
            $table->tinyInteger('likert35');
            $table->tinyInteger('likert36');
            $table->tinyInteger('likert37');
            $table->tinyInteger('likert38');
            $table->tinyInteger('likert39');
            $table->tinyInteger('likert40');
            $table->tinyInteger('likert41');
            $table->tinyInteger('likert42');
            $table->tinyInteger('likert43');
            $table->tinyInteger('likert44');
            $table->tinyInteger('likert45');
            $table->tinyInteger('likert46');
            $table->tinyInteger('likert47');
            $table->tinyInteger('likert48');
            $table->tinyInteger('likert49');
            $table->tinyInteger('likert50');
            $table->tinyInteger('likert51');
            $table->tinyInteger('likert52');
            $table->tinyInteger('likert53');
            $table->tinyInteger('likert54');
            $table->tinyInteger('likert55');
            $table->tinyInteger('likert56');
            $table->tinyInteger('likert57');
            $table->tinyInteger('likert58');
            $table->tinyInteger('likert59');
            $table->tinyInteger('likert60');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests');
    }
}
