<!DOCTYPE html>
<html lang="en" xmlns:font-family="http://www.w3.org/1999/xhtml" xmlns:color="http://www.w3.org/1999/xhtml">
<head>
    <title>RECRUITER</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/cv.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
          integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <script type="application/javascript" src="js/cv.js"></script>


</head>
<body>
<h1> Responsive Material Design Form
    <small>Material Design delivers a cleaner and flatter user interface</small>
</h1>
<section class="contact-wrap">
    <form action="" class="contact-form">
        <div class="col-sm-6">
            <div class="input-block">
                <label for="">First Name</label>
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="input-block">
                <label for="">Last Name</label>
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-12">
            <div class="input-block">
                <label for="">Email</label>
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-12">
            <div class="input-block">
                <label for="">Message Subject</label>
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-12">
            <div class="input-block textarea">
                <label for="">Drop your message here</label>
                <textarea rows="3" type="text" class="form-control"></textarea>
            </div>
        </div>
        <div class="col-sm-12">
            <button class="square-button">Send</button>
        </div>
    </form>
</section>

<!-- follow me template -->
<div class="made-with-love">
    Made with
    <i>♥</i> by
    <a target="_blank" href="https://codepen.io/nikhil8krishnan">Nikhil Krishnan</a>
</div>

</body>
</html>
