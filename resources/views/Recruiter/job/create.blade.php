@extends('admin.layouts.master')


@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1></h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Icons</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        @if(count($errors) > 0 )
            @foreach($errors->all() as $error)
                <script>
                    swal(" OHHH!", "{{ $error }}", "error");
                </script>
            @endforeach
        @endif


        <section class="content">
            <div class="container-fluid">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">CREATE JOB POST </h3>
                    </div>
                    <div class="card-body">
                        <form id="create_form"  role="form" method="POST" action="{{ route('job.store') }}" enctype="multipart/form-data" onsubmit=" return validateForm()">
                            {{ csrf_field() }}
                            <div class="box-body" style="height: 100%;">
                                <div class="form-group">
                                    <label for="title">Job Title & Position </label>
                                    <input name="name" type="text" class="form-control" id="name" value="{{ old('name') }}" placeholder="Example: HR head position at the COCA COLA Company">
                                    <span class="error" id="title_error" aria-live="polite"></span>
                                </div>
                                <div class="form-group">
                                    <label for="title">Estimated Salary </label>
                                    <input name="salary" type="text" class="form-control" id="link" value="{{ old('link') }}" placeholder="It is recommended to state estimated salary for the Position in thousand(s)">
                                    <span class="error" id="title_error" aria-live="polite"></span>
                                </div>
                                <div  class="form-group" style="padding-top: 10px;">
                                    <label for="description"> Description: <p12 >( Company's (Name , Address , Contact) Skills Required , Links of Previous Work  )</p12></label>
                                    <textarea name="description" class="form-control summernote" cols="30" rows="13" id="summernote" placeholder="Tell us about your  1- Company (NAME  ADDRESS STATE CONTACT DETAILS) 2- Skills Required for the Job 3- Any sample work required ? 4- What kind of a candidate are you looking for ?" >{{ old('description') }}</textarea>
                                    <span class="error" id="description_error" aria-live="polite"></span>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection




@section('extra_scripts')

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();

            $('.summernote').summernote({
                height: 200,
            });


            $('.custom-file-input').on('change', function () {
                let fileName = $(this).val().split('\\').pop();
                $(this).next('.custom-file-label').addClass("selected").html(fileName);
            });
        });


        function strip(html) {
            var tmp = document.createElement("DIV");
            tmp.innerHTML = html;
            return tmp.textContent || tmp.innerText || "";
        }



    </script>
@stop


