@extends("admin.layouts.master")

@section("content")

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">

                    <div class="card">

                        <!-- /.card-header -->

                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-8" style="margin-top: 40px!important;">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">User Information</h3>

                            <div class="card-tools">
                            </div>
                        </div>
                        <!-- /user name -->
                        <div class="card-body p-0">
                            <div class="mailbox-read-info">
                                <h5>User Name</h5>
                                <h6>{{ $user->name }}</h6>
                            </div>
                            <!-- /.mailbox-read-info -->
                            <div class="mailbox-controls with-border text-center">


                            </div>
                            <!-- /.mailbox-controls -->
                            <div class="mailbox-read-message">
                                <p>Description: {{ $user->description }}</p>

                                <p>Skills: {{ $user->skills }}</p>

                                <p>Expected Salary: {{ $user->salary }}</p>

                                <p>Mobile Number: {{ $user->mobile_number }}</p>

                                <p>Work Email: {{ $user->work_email }}</p>
                                @if(isset($user->test))
                                <p>Test Score: </p>
                                <ul>
                                    @foreach($user->recentPersonalities as $personality)
                                        <li style="padding-left:5px">{{$personality->name}}</li>
                                    @endforeach
                                </ul>
                                @endif

                            </div>
                            <!-- /.mailbox-read-message -->
                        </div>
                        <!-- /.card-body -->
                        <!-- /.card-footer -->
                        {{--<div class="card-footer">--}}
                            {{--<div class="float-right">--}}
                                {{--<button type="button" class="btn btn-default"><i class="fa fa-reply"></i><a--}}
                                        {{--href="{{ route("apply.now", $job->id) }}"> Submit Resume</a></button>--}}

                            {{--</div>--}}

                        {{--</div>--}}
                        <!-- /.card-footer -->
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </section>

@stop
