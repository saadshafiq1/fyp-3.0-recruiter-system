@extends('admin.layouts.master')



@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DashBorad</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>

                        </ol>
                    </div>
                </div>
            </div>
        </section>

        @if(count($errors) > 0 )
            @foreach($errors->all() as $error)
                <script>
                    swal("AH OH!", "{{ $error }}", "error");
                </script>
            @endforeach
        @endif

        @if(Session::has('image_created'))
            <script>
                swal("Great!", "{{session('image_created')}}", "success");
            </script>
        @endif
        @if(Session::has('job_created'))
            <script>
                swal("Great!", "{{session('job_created')}}", "success");
            </script>
        @endif

        @if(Session::has('image_updated'))
            <script>
                swal("Great!", "{{session('image_updated')}}", "success");
            </script>
        @endif


        <section class="content">
            <div class="container-fluid">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <div class="row">

                            <h2>Job Requests</h2>
                            <div class="px-1"></div>

                            <a class="message-modal-r" href="#" >
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped" id="table">
                            <thead>
                            <tr>
                                <th scope="col">SR</th>
                                <th scope="col">Job Position</th>
                                <th scope="col">Job Description</th>
                                <th scope="col">Users job request</th>
                                <th scope="col">Delete Post</th>


                            </tr>
                            </thead>
                            <tbody>

                        @if(isset($recruiter->jobs))
                            @foreach($recruiter->jobs as $job)
                                <tr class="del" data-id="{{$job->id}}">
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $job->job_position }}</td>
                                    <td>{{ str_limit(strip_tags($job->job_description,50)) }}</td>
                                    <td>
                                    @foreach($job->user->sortByDesc('experience')    as $user)
                                        <li>
                                            <a href="{{route("specific.user",$user->id)}}">{{ $user->name }}</a>
                                        </li>
                                        @endforeach
                                    </td>

                                    {{--<td><button type="submit" class="btn btn-danger">Delete</button></td>--}}
                                    <td><button style="padding-bottom: 4px;padding-top: 4px;background-color: #b21f2d;padding-right: 15px;padding-left: 15px;border-color:#b21f2d;box-shadow: none; border-radius: 6px;"> <a   style="color: white" href= "{{action('RecruiterJobsController@destroy',$job->id)}}">Delete</a></button></td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>

    </div>



    {{--<div class="modal fade" id="image-modal"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
        {{--<div class="modal-dialog" role="document">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header">--}}
                    {{--<h5 class="modal-title" id="exampleModalLabel">Upload Image</h5>--}}
                    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                        {{--<span aria-hidden="true">&times;</span>--}}
                    {{--</button>--}}
                {{--</div>--}}
                {{--<div class="modal-body">--}}
                    {{--<form action="{{ route('image.store') }}" onsubmit="return validateForm()" method="post"  enctype="multipart/form-data" >--}}
                        {{--{{ csrf_field() }}--}}
                        {{--<div class="form-group">--}}
                            {{--<input type="file" class="image" name="image">--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<input class="btn btn-primary" type="submit" name="submit" value="Create">--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@stop

@section('extra_scripts')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
    <script>
        function openModal(myObj) {
            var title = $(myObj).data("title");
            var id = $(myObj).data("id");
            var description = $(myObj).data("content");
            var icon = $(myObj).data("icon");


            $(".description1").summernote("code",description);
            $(".title-edit").val(title);
            $("#default1").val(icon);
            $("#default1").siblings('button').find("i").removeClass();
            $("#default1").siblings('button').find("i").addClass(icon);
            $("#service-id").val(id);
            $("#service-modal-update").modal("show");
        }




        $(document).ready(function () {
            $('.js-example-basic-multiple').select2();
            $('.table').DataTable();


            $('#target1').on('change', function (e) {
                $("#default1").val(e.icon);
            });

            $("#target").on("change", function(e){
                $("#default").val(e.icon);
            });

        });
        $('#view-image').click(function () {
            $('#image-modal').modal();
        });

        $('#view').click(function () {
            $('#service-modal').modal();

            $('.summernote').summernote({
                height: 200,
            });
        });



        function strip(html) {
            var tmp = document.createElement("DIV");
            tmp.innerHTML = html;
            return tmp.textContent || tmp.innerText || "";
        }

        function validateForm() {

            if ($(".image")[0].files.length == 0) {
                swal('Not Saved', 'Image is Required', 'error');
                return false;
            }

            var file_size = $('.image')[0].files[0].size;
            if (file_size > 2097152) {
                swal('Image size not greater then  2mb', 'Error!');
                return false;
            }
            return true;
        }

        function validateEditForm() {
            var title = $('.title-edit').val().trim();
            var icon = $(".icon-chooser-edit").val().trim();
            $(".temp-textarea").html($('.descripton1').val());
            var description = $('.description1').val();
            if (title.length == 0) {
                swal('Not Saved', 'Title is Required', 'error');
                return false
            }
            if (strip(description).trim().length == 0) {
                swal('Not Saved', 'Description is Required', 'error');
                return false
            }
            if (icon.length == 0) {
                swal('Not Saved', 'Icon is required', 'error');
                return false
            }
            return true;
        }



        function delFunction(id){
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this Image!",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "Cancel!",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Yes, delete it!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            })
                .then(isConfirm => {
                    if (isConfirm) {
                        $.ajax({
                            type: 'post',
                            data: {
                                id: id,
                                _token:CSRF_TOKEN,
                            },
                            dataType: 'JSON',
                            success: function (data) {
                                console.log(data);
                                if (data.status == 200) {
                                    swal(" Image has been deleted!", {
                                        icon: "success",
                                    });
                                    $(".del[data-id="+id+"]").remove();
                                }
                                if (data.status == -1) {
                                    swal("Cancelled", "It's safe.", "error");
                                }
                            },
                        });
                    } else {
                        swal("Cancelled", "It's safe.", "error");
                    }
                });
        }
    </script>
@stop


