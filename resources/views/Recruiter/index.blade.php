@extends('admin.layouts.master')


@section('content')
    <style>
        .fc-day-number{
            color:white;
        }
        .fc-title{
            background: #f78536;
        }
        .fc-content{
            background: #f78536;
        }
        .fc-event{
            border: 1px solid #f78536;

            background-color: #f78536;
        }
        .fc-month-button{
            display:none
        }
        .fc-agendaWeek-button{
            display:none;
        }
        .fc-agendaDay-button{
            display:none
        }
        .fc-day-header{
            background:#2b4450 ;
            color:white;
        }
        .fc-center{
            color: white;
        }
        .fc-today{
            color:black;
            background-color: #fff!important;
        }
        .fc-prev-button{
            background-color:#2b4450 ;
        }
        #calendar-a1KbvBMC{
            color:#2b4450
        }
    </style>
    <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700);




        .animation {
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease;
        }


        .containers {



            /* Set the depth of the elements */
            -webkit-perspective: 800px;
            -moz-perspective: 800px;
            -o-perspective: 800px;
            perspective: 800px;
        }

        .containers:first-child {
            margin-left: 0;
        }

        .cards {



            /* Set the transition effects */
            -webkit-transition: -webkit-transform 0.4s;
            -moz-transition: -moz-transform 0.4s;
            -o-transition: -o-transform 0.4s;
            transition: transform 0.4s;
            -webkit-transform-style: preserve-3d;
            -moz-transform-style: preserve-3d;
            -o-transform-style: preserve-3d;
            transform-style: preserve-3d;
        }

        .cards.flipped {
            -webkit-transform: rotateY( 180deg );
            -moz-transform: rotateY( 180deg );
            -o-transform: rotateY( 180deg );
            transform: rotateY( 180deg );
        }

        .cards .front,
        .cards .back {
            display: block;
            height: 100%;
            width: 100%;
            line-height: 260px;
            color: white;
            text-align: center;
            font-size: 4em;
            position: absolute;
            -webkit-backface-visibility: hidden;
            -moz-backface-visibility: hidden;
            -o-backface-visibility: hidden;
            backface-visibility: hidden;

            box-shadow: 3px 5px 20px 2px rgba(0, 0, 0, 0.25);
        }

        .cards.flipped .fronts{
            display: none!important;
        }
        .cards.flipped .col-md-6{
            display: none!important;
        }

        .cards .back {
            width: 94%;
            padding-left: 3%;
            padding-right: 3%;
            font-size: 16px;
            text-align: left;
            line-height: 25px;

        }

        .formItems:first-child {
            margin-top: 20px;
        }

        .cards .back label {
            display: inline-block;
            width: 70px;
            text-align: left;
        }

        .cards .fronts {
            background: ;
        }

        .cards .back {
            background: #2b4450;
            -webkit-transform: rotateY( 180deg );
            -moz-transform: rotateY( 180deg );
            -o-transform: rotateY( 180deg );
            transform: rotateY( 180deg );
        }

        .containers:first-child .cards .fronts {
            background: #228653;
        }

        .containers:first-child .cards .back {
            background: #007539;
        }

        .cardTitles {
            font-size: 1.4em;
            line-height: 1.2em;
            margin: 0;
        }

        .contents {
            padding: 4%;
            font-weight: 100;
            text-align: left;
        }

        button.btnSend {
            display: inline-block;
            min-width: 100px;
            padding: 3px 5px;
            margin-top: 10px;
            font-weight: bold;
            text-transform: uppercase;
            text-align: center;
            color: #03446A;
            background: #fff;
            border: 0;
            border-radius: 3;
        }
    </style>
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div>
                </div>
            </div>
        </div>
        @if(count($errors) > 0 )
            @foreach($errors->all() as $error)
                <script>
                    swal("AH OH!", "{{ $error }}", "error");
                </script>
            @endforeach
        @endif

        @if(Session::has('created'))
            <script>
                swal("Great!", "{{session('created')}}", "success");
            </script>
        @endif

        @if(Session::has('updated'))
            <script>
                swal("Great!", "{{session('updated')}}", "success");
            </script>
        @endif

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-pencil-alt"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Projects</span>
                                <span class="info-box-number"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-book"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-book">Testimonials</span>
                                <span class="info-box-number"></span>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix hidden-md-up"></div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-success elevation-1"><i class="fa fa-shopping-cart"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Services</span>
                                <span class="info-box-number"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-warning elevation-1"><i class="fa fa-users"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Messages</span>
                                <span class="info-box-number"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="service-modal"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Create Booking</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            <!--        <div class="col-md-6">
                    <div class="cards card-primary">
                        <div class="card-body p-4" style="background:#2b4450">
                            <button class="btn btn-info" onclick="openModal()">Add Booking</button>
                            <div class="fronts">
                </div>
            </div>
            <div class="back" style="height: 718px; width: 100%">
                <div class="contents">
                    <h3 class="cardTitle">Quem eu sou?</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sodales, mi ac vulputate blandit, libero nulla bibendum quam.</p>
                </div>
            </div>
        </div>
    </div> -->
            </div>
        </section>
    </div>



@stop
@section('extra_head')


    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

@stop

@section('extra_scripts')
    <script>
        $('.cards').click(function(){
            $(this).toggleClass('flipped');
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>

    <script>

        function validateEditForm() {
            var name = $('#name-edit').val().trim();
            var title = $('#title-edit').val().trim();
            var date = $('#date-edit').val().trim();

            if (name.length == 0) {
                swal('Not Saved', 'Name is Required', 'error');
                return false
            }
            if (title.length == 0) {
                swal('Not Saved', 'Title is Required', 'error');
                return false
            }
            if (date.length == 0) {
                swal('Not Saved', 'Date is Required', 'error');
                return false
            }
            return true;
        }

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".fc-next-button").on("click", function(){
                console.log('next-click');
                $(".fc-content").off();
                $(".fc-content").click(function(){
                    var td = $(this).closest('td');
                    // var elemDate = "2019-03-04";
                    try {
                        var pos = $(td).prevAll().length;
                        // console.log(pos);
                        var num = $(td).closest('table').children('thead').children("tr").children("td")[pos];
                        if(!num){
                            var num = $($(td).closest('.fc-row').find("table")[1]).children("thead").find("td")[pos];
                        }
                        elemDate = $(num).data("date");
                    }
                    catch(e){
                        elemDate = $(td).data("date");
                    }
                    var title,name;
                    title = $(this).children(".fc-title").html().split("<br>")[0];
                    name = $(this).children(".fc-title").html().split("<br>")[1];
                    var myid = $(this).children('.fc-title').data("myid");

                    // console.log(title, name, myid);

                    var id = $(this).children(".fc-title").data("myid");


                    $(".deleteBtn").attr("onClick", 'myFunction('+myid+')');

                    // console.log(elemDate);
                    // $(this).index()
                    $("#name-edit").val(name);
                    $("#title-edit").val(title);
                    $("#date-edit").val(elemDate);
                    $("#edit-id").val(myid);
                    $("#edit-modal").modal("show")

                });
            });
            $(".fc-prev-button").on("click", function(){
                $(".fc-content").off();
                $(".fc-content").click(function(){
                    var td = $(this).closest('td');
                    // var elemDate = "2019-03-04";
                    try {
                        var pos = $(td).prevAll().length;
                        // console.log(pos);
                        var num = $(td).closest('table').children('thead').children("tr").children("td")[pos];
                        if(!num){
                            var num = $($(td).closest('.fc-row').find("table")[1]).children("thead").find("td")[pos];
                        }
                        elemDate = $(num).data("date");
                    }
                    catch(e){
                        elemDate = $(td).data("date");
                    }
                    var title,name;
                    title = $(this).children(".fc-title").html().split("<br>")[0];
                    name = $(this).children(".fc-title").html().split("<br>")[1];
                    var myid = $(this).children('.fc-title').data("myid");
                    var id = $(this).children(".fc-title").data("myid");
                    $(".deleteBtn").attr("onClick", 'myFunction('+myid+')');
                    // console.log(elemDate);
                    // $(this).index()
                    $("#name-edit").val(name);
                    $("#title-edit").val(title);
                    $("#date-edit").val(elemDate);
                    $("#edit-id").val(myid);
                    $(this).toggleClass('flipped');

                });
            });
        });
        {{--$('#fc-event-colors').fullCalendar({--}}

        {{--header: {--}}
        {{--themeSystem: 'bootstrap4',--}}
        {{--left: 'prev,next today',--}}
        {{--center: 'title',--}}
        {{--right: 'month'--}}
        {{--},--}}
        {{--defaultDate: '{{\Carbon\Carbon::now()}}',--}}
        {{--businessHours: true, // display business hours--}}
        {{--editable: true,--}}
        {{--events: [--}}
        {{--@foreach($booking as $book)--}}
        {{--{--}}
        {{--title : "{{ $book['booiking_name'] }}\n{{$book['title']}}\n",--}}
        {{--myid: "{{ $book->id }}",--}}
        {{--googleCalendarId:"hi",--}}
        {{--className: "myclass",--}}
        {{--start: '{{$book->start_time}}',--}}
        {{--color: '#28a745',--}}
        {{--editable : false--}}

        {{--},--}}
        {{--@endforeach--}}

        {{--],--}}
        {{--slotLabelFormat:'H(:mm)'--}}
        {{--});--}}
    </script>
    <script>
        function openModal(myObj) {
            var title = $(myObj).data("title");
            var id = $(myObj).data("id");
            var description = $(myObj).data("content");
            var icon = $(myObj).data("icon");



            $(".title-edit").val(title);
            $("#default1").val(icon);
            $("#default1").siblings('button').find("i").removeClass();
            $("#default1").siblings('button').find("i").addClass(icon);
            $("#service-id").val(id);
            $("#service-modal").modal("show");
        }



        $(document).ready(function () {

            $('#target1').on('change', function (e) {
                $("#default1").val(e.icon);
            });

            $("#target").on("change", function(e){
                $("#default").val(e.icon);
            });

            $('#view').click(function () {
                $('#service-modal').modal();

                $('.summernote').summernote({
                    height: 200,
                });
            });

            $("#view-information").click(function(){
                $("#information-modal").modal("show")
            });
            // $(".fc-content").click(function(){
            //
            //     var td = $(this).closest('td');
            //     // var elemDate = "2019-03-04";
            //     try {
            //         var pos = $(td).prevAll().length;
            //         // console.log(pos);
            //         var num = $(td).closest('table').children('thead').children("tr").children("td")[pos];
            //         if(!num){
            //             var num = $($(td).closest('.fc-row').find("table")[1]).children("thead").find("td")[pos];
            //         }
            //         elemDate = $(num).data("date");
            //     }
            //     catch(e){
            //         elemDate = $(td).data("date");
            //     }
            //     var title,name;
            //     title = $(this).children(".fc-title").html().split("<br>")[0];
            //     name = $(this).children(".fc-title").html().split("<br>")[1];
            //     var myid = $(this).children('.fc-title').data("myid");
            //
            //     // console.log(title, name, myid);
            //
            //     var id = $(this).children(".fc-title").data("myid");
            //
            //
            //     $(".deleteBtn").attr("onClick", 'myFunction('+myid+')');
            //
            //     // console.log(elemDate);
            //     // $(this).index()
            //     $("#name-edit").val(name);
            //     $("#title-edit").val(title);
            //     $("#date-edit").val(elemDate);
            //     $("#enddate-edit").val(elemDate);
            //     $("#edit-id").val(myid);
            //     $("#edit-modal").modal("show")
            //
            // });
        });


        function myFunction(id) {
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this Booking!",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "Cancel!",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Yes, delete it!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            })
                .then(isConfirm => {
                    if (isConfirm) {
                        $.ajax({
                            type: 'post',
                            data: {
                                id: id,
                                _token:CSRF_TOKEN,
                            },
                            dataType: 'JSON',
                            success: function (data) {
                                console.log(data);
                                if (data.status == 200) {
                                    swal(" Booking has been deleted!", {
                                        icon: "success",
                                    });
                                    $(".fc-event-container[data-myid="+id+"]").remove();
                                    $('#edit-modal').modal("hide");
                                }
                                if (data.status == -1) {
                                    swal("Cancelled", "It's safe.", "error");
                                }
                            },
                        });
                    } else {
                        swal("Cancelled", "It's safe.", "error");
                    }


                });
        }


        function strip(html) {
            var tmp = document.createElement("DIV");
            tmp.innerHTML = html;
            return tmp.textContent || tmp.innerText || "";
        }

        function validateForm() {
            var name = $('#name').val().trim();
            var title = $("#title").val().trim();
            var date = $("#date").val().trim();



            if (name.length == 0) {
                swal('Not Saved', 'Name is Required', 'error');
                return false
            }

            if (date.length == 0) {
                swal('Not Saved', 'Title is required', 'error');
                return false
            }
            if (title.length == 0) {
                swal('Not Saved', 'Date is required', 'error');
                return false
            }
            return true;
        }




        function delFunction(id){
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this ID!",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "Cancel!",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Yes, delete it!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            })
                .then(isConfirm => {
                    if (isConfirm) {
                        $.ajax({
                            type: 'post',
                            data: {
                                id: id,
                                _token:CSRF_TOKEN,
                            },
                            dataType: 'JSON',
                            success: function (data) {
                                console.log(data);
                                if (data.status == 200) {
                                    swal(" ID has been deleted!", {
                                        icon: "success",
                                    });
                                    $(".del[data-id="+id+"]").remove();
                                }
                                if (data.status == -1) {
                                    swal("Cancelled", "It's safe.", "error");
                                }
                            },
                        });
                    } else {
                        swal("Cancelled", "It's safe.", "error");
                    }
                });
        }

        function deleteFunction(id){
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this Project!",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "Cancel!",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Yes, delete it!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            })
                .then(isConfirm => {
                    if (isConfirm) {
                        $.ajax({
                            url: "",
                            type: 'post',
                            data: {
                                id: id,
                                _token:CSRF_TOKEN,
                            },
                            dataType: 'JSON',
                            success: function (data) {
                                console.log(data);
                                if (data.status == 200) {
                                    swal(" ID has been deleted!", {
                                        icon: "success",
                                    });
                                    $(".del[data-id="+id+"]").remove();
                                }
                                if (data.status == -1) {
                                    swal("Cancelled", "It's safe.", "error");
                                }
                            },
                        });
                    } else {
                        swal("Cancelled", "It's safe.", "error");
                    }
                });
        }
    </script>
    <script>
        $(document).ready(function(){
            $(".fc-widget-content td" ).click(function(){
                console.log("td clicked");
            })
        });
    </script>
@stop











