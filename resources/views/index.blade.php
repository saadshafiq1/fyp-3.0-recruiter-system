<!DOCTYPE html>
<html lang="en" xmlns:font-family="http://www.w3.org/1999/xhtml" xmlns:color="http://www.w3.org/1999/xhtml">
<head>
    <title>RECRUITER</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/index.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
          integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


</head>
<body>

{{--<div class="container-fluid ">--}}
    {{--<div id="bar2" class="col-md-4">--}}
        {{--<div id="bar" style="padding-top: 8px">--}}
            {{--<ul id="bar1">--}}
                {{--<li><a href="#"> PERSONALITY </a></li>--}}
                {{--<li><a href="#"> BASED  </a></li>--}}
                {{--<li><a href="#"> E-RECRUITMENT </a></li>--}}
                {{--<li><a href="#"> PORTAL  </a></li>--}}
            {{--</ul>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
<nav class="navbar navbar-default navi" id="bar3" style="padding-top: 10px;">
    <div class="container-fluid " style="padding: 0px;margin: 0px">
        <div class="nav">
            <div class="nav-header">
                <div class="">
                    <a class="navbar-brand" href="#" style="padding: 0px"><img style="width: 30%;
    height: 105%; padding-left: 10px" class="image1" src="{{URL::to ('images/logo.png')}}"></a>
                </div>

            </div>
            <div class="nav-btn">
                <label for="nav-check">
                    <span></span>
                    <span></span>
                    <span></span>
                </label>
            </div>
            <input type="checkbox" id="nav-check">
            <div class="nav-links nav23 "id="head1">
                {{--<a href="{{ route('recruiter.register') }}" target="_blank" style="border:  solid 1px rgba(0.5,0.5,0.5,0.5); border-radius:5px ">RECRUITER Registration</a>--}}
                {{--<a href="" target="_blank"  style="border:  solid 1px rgba(0.5,0.5,0.5,0.5); border-radius:5px ">APPLY FOR A JOB</a>--}}
                <a href="{{ route('login') }}" target="_blank" class="button">CANDIDATE</a>
                <a href="{{ route('admin.login') }}" class="button" target="_blank" >RECRUITER</a>
            </div>
        </div>
    </div>
</nav>
{{--<div class="container-fluid" style="padding-top: 20px; padding-left: 30px">--}}


    {{--<!---search bar --->--}}
    {{--<div class="search-box" style="float: right ; display: inline ; padding-bottom: 70px; margin-top:-8px ">--}}

        {{--<a href="#contact"><img src="images/cart.svg"--}}
                                {{--style=" width: 40px; height: 40px ; transition-delay: 0.4s; padding-right: 10px"></a>--}}
        {{--<a href="#contact"><img src="images/file.svg"--}}
                                {{--style=" width: 30px; height: 30px ; transition-delay: 0.4s; padding-right: 10px"></a>--}}
        {{--<a href="#contact"><img src="images/heart.svg"--}}
                                {{--style=" width: 40px; height: 40px ; transition-delay: 0.4s; padding-right: 10px"></a>--}}
        {{--<input id="src" class="search-txt" style="width: 0px; " type="text" placeholder=" Type to search ">--}}
        {{--<a class="search-btn" href="#">--}}
            {{--<i class="fas fa-search" style="color: white"></i>--}}
        {{--</a>--}}
    {{--</div>--}}


{{--</div>--}}
</div>
</div>
<div>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators align-content-xl-stretch">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" >
            <div class="item active">
                <img src="images/job2.jpg" alt="html" style="width:100%;">
            </div>

            <div class="item">
                <img  src="images/job3.jpg" alt="java" style="width:100%;">
            </div>

            <div class="item">
                <img  src="images/job1.jpg" alt="www" style="width:100%;">
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
{{--<div class="container-fluid " id="image1"style="padding: 0px">--}}
    {{--<div class="imag container-fluid row">--}}
        {{--<div class="col-md-12 " style="display: flex;justify-content: center"></div>--}}
        {{--<div class="col-md-4">--}}
            {{--<a href="#"><img src="images/show.gif" style="height: 230px;width: 400px;border-radius:8px;"></a></div>--}}
        {{--<div class="col-md-4">--}}
            {{--<a href="#"><img src="images/show1.gif" style="height: 230px;width: 400px;border-radius:8px;"></a></div>--}}
        {{--<div class="col-md-4">--}}
            {{--<a href="#"><img src="images/gif.gif" style="height: 230px;width: 400px;border-radius:8px;"></a></div>--}}
    {{--</div>--}}

{{--</div>--}}
<nav class="navbar navbar-expand-lg mynav"style="margin: 0; padding-left: 0;padding-right: 0; margin-bottom: 40px">
    <h class="heading">LOOKING FOR <p style="color: #b21517; display: inline"> A BETTER JOB & </p> CAREER ?</h>
</nav>
<div class="container-fluid"style="padding: 0px">
    <div id="tabs" style=" position: relative;z-index: 0;">
        <ul class="nav nav-pills" id="nav-pills "
            style="border-bottom: #4e555b;display: flex ; position: relative;z-index: 1;font-family: 'Arial Black';justify-content: center; font-size: large;background-color:transparent;">
            <li><a href="#tabs-2"> TRENDING JOBS </a></li>
            <li><a href="#tabs-1"> MOST SEARCHED COMPANIES  </a></li>

        </ul>

        <div id="tabs-1" class="container-fluid">
            <div id="particles-js1" style="margin: -15px ; height: 700px;padding-top: 25px"></div>

            <!-- stats - count particles -->
            <div class="count-particles container" style="padding-top: 45px">
                <span class="js-count-particles">--</span> particles
            </div>

            <div class="row">
                <div class="col-md-12">

                    <div id="mdb-lightbox-uii"></div>

                    <div class="mdb-lightbox no-margin">
                        <figure class="col-md-4">
                            <div class="container-fluid">
                                <a>
                                    <img class="picture" src="images/t.gif"/>

                                </a>
                            </div>

                        </figure>

                        <figure class="col-md-4">
                            <div class="container-fluid">
                                <a>
                                    <img id="picture4"class="picture" src="images/m.gif"/>

                                </a>
                            </div>
                        </figure>

                        <figure class="col-md-4">
                            <div class="container-fluid">
                                <a>
                                    <img id="picture3" class="picture" src="images/s.gif"/>

                                </a>
                            </div>
                        </figure>
                        {{--<figure class="col-md-4">--}}
                        {{--<div class="container-fluid">--}}
                        {{--<a>--}}
                        {{--<img id="picture4"class="picture" src="images/4ht.jpg"/>--}}
                        {{--<button class=" btnn" id="button3"--}}
                        {{--style="font-family: 'Arial';">ADD TO CART--}}
                        {{--</button>--}}
                        {{--</a>--}}
                        {{--</div>--}}
                        {{--</figure>--}}
                        {{--<figure class="col-md-4">--}}
                        {{--<div class="container-fluid">--}}
                        {{--<a>--}}
                        {{--<img id="picture5" class="picture" src="images/7th.jpg"/>--}}
                        {{--<button class="btnn" id="button4"--}}
                        {{--style="font-family: 'Arial';">ADD TO CART--}}
                        {{--</button>--}}
                        {{--</a>--}}
                        {{--</div>--}}
                        {{--</figure>--}}
                        {{--<figure class="col-md-4">--}}
                        {{--<div class="container-fluid">--}}
                        {{--<a>--}}
                        {{--<img id="picture6" class="picture"src="images/5ht.jpg"/>--}}
                        {{--<button class=" btnn" id="button5"--}}
                        {{--style="font-family: 'Arial';">ADD TO CART--}}
                        {{--</button>--}}
                        {{--</a>--}}
                        {{--</div>--}}
                        {{--</figure>--}}
                    </div>

                </div>
            </div>

        </div>
        <div id="tabs-2" class="container-fluid">
            <div id="particles-js1" style="margin: -15px ; height: 700px;padding-top: 25px"></div>

            <!-- stats - count particles -->
            <div class="count-particles container" style="padding-top: 45px">
                <span class="js-count-particles">--</span> particles
            </div>

            <div class="row">
                <div class="col-md-12">

                    <div id="mdb-lightbox-uii"></div>

                    <div class="mdb-lightbox no-margin">
                        <figure class="col-md-4">
                            <div class="container-fluid">
                                <a>
                                    <img class="picture" src="images/h.jpg"/>
                                    <button class=" btnn " id="button"
                                            style="font-family: 'Arial';">HEALTH AND NURSING
                                    </button>
                                </a>
                            </div>

                        </figure>

                        <figure class="col-md-4">
                            <div class="container-fluid">
                                <a>
                                    <img id="picture4"class="picture" src="images/banking.jpg"/>
                                    <button class=" btnn" id="button3"
                                            style="font-family: 'Arial';">BANKING & FINANCE
                                    </button>
                                </a>
                            </div>
                        </figure>

                        <figure class="col-md-4">
                            <div class="container-fluid">
                                <a>
                                    <img id="picture3" class="picture" src="images/cs.png"/>
                                    <button class=" btnn" id="button2"
                                            style="font-family: 'Arial';">IT & COMPUTER
                                    </button>
                                </a>
                            </div>
                        </figure>

                    </div>

                </div>
            </div>

        </div>
        <nav class="navbar navbar-expand-lg mynav" style="padding: 0px; margin-top:76px; margin-left: 0px; margin-right: 0px">
            <h class="heading" >WE FIND YOU JOB AT  <p style="color: #b21517;letter-spacing: 3px; display: inline">  GREAT COMPANIES</p>    </h>
        </nav>
        <div id="taby" style=" position: relative;z-index: 0; background-color: white;">
            <ul class="nav nav-pills"
                style="border-bottom: #4e555b;display: flex ; position: relative;z-index: 1;padding-bottom:5px ;font-family: 'Arial Black';justify-content: center; font-size: large;background-color:snow;">
                {{--<li><a href="#tabs2">CUSTOM ICONS</a></li>--}}
                <li><a href="#tabs3"> WHY US? </a></li>
                <li><a href="#tabs1"> CUSTOMER REVIEWS </a></li>

            </ul>

            <div id="tabs1" class="container-fluid"style="padding-top: 25px">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div id="carousel" class="carousel slide" data-ride="carousel" data-interval="0">
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <img src="images/r1.jpg">
                                    </div>
                                    <div class="item">
                                        <img src="images/r2.jpg">
                                    </div>
                                    <div class="item">
                                        <img src="images/r3.jpg">
                                    </div>
                                    <div class="item">
                                        <img src="images/r1.jpg">
                                    </div>
                                    <div class="item">
                                        <img src="images/r2.jpg">
                                    </div>
                                    <div class="item">
                                        <img src="images/r3.jpg">
                                    </div>
                                    <div class="item">
                                        <img src="images/r1.jpg">
                                    </div>
                                    <div class="item">
                                        <img src="images/r2.jpg">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div id="thumbcarousel" class="carousel slide" data-interval="false">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <div data-target="#carousel" data-slide-to="0" class="thumb"><img src="images/r1.jpg"></div>
                                            <div data-target="#carousel" data-slide-to="1" class="thumb"><img src="images/r2.jpg"></div>
                                            <div data-target="#carousel" data-slide-to="2" class="thumb"><img src="images/r3.jpg"></div>
                                            <div data-target="#carousel" data-slide-to="3" class="thumb"><img src="images/r1.jpg"></div>
                                        </div><!-- /item -->
                                        <div class="item">
                                            <div data-target="#carousel" data-slide-to="4" class="thumb"><img src="images/r1.jpg"></div>
                                            <div data-target="#carousel" data-slide-to="5" class="thumb"><img src="images/r2.jpg"></div>
                                            <div data-target="#carousel" data-slide-to="6" class="thumb"><img src="images/r3.jpg"></div>
                                            <div data-target="#carousel" data-slide-to="7" class="thumb"><img src="images/r1.jpg"></div>
                                        </div><!-- /item -->
                                    </div><!-- /carousel-inner -->
                                    <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                    </a>
                                    <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </a>
                                </div> <!-- /thumbcarousel -->
                            </div><!-- /clearfix -->
                        </div> <!-- /col-sm-6 -->
                        <div class="col-sm-6">
                            <h2 style="color: #144E80;padding-top: 25px; font-family: 'Arial Black'">THESE CUSTOMERS ARE HAPPY </h2>
                            <h3 style="color: #144E80">Here's what some of our customers are saying</h3>
                            <p>WE DO THE HEAVY LIFTING FOR YOU. OUR PLATFORM PUTS YOUR HARD-TO-FILL JOBS IN FRONT OF MORE RECRUITERS THAN ANYONE ELSE.

                                NAME YOUR FEE. MAXIMUM REACH. ONE CONTACT. GET RESULTS. START HIRING.</p>
                            <p>SEND YOUR CV TO THE LARGEST LIST OF RECRUITERS ON THE PLANET. BROWSE 6+ MILLION JOBS AND CREATE JOB ALERTS.

                                WE REPRESENT YOU DIRECTLY IF YOU'RE A MATCH FOR OUR JOB MARKET OPENINGS.</p>

                        </div> <!-- /col-sm-6 -->
                    </div> <!-- /row -->
                </div> <!-- /container -->

            </div>
            {{--<div id="tabs2" class="container-fluid">--}}

                {{--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">--}}
                {{--<div class="k-container flex-center flex-mobile-column-alt">--}}
                    {{--<div class="flex-center flex-grow">--}}
                        {{--<h3 class="text-uppercase;"style="color: #144E80; font-family: 'Arial Black'"></h3>--}}
                    {{--</div>--}}
                    {{--<div class="carousel-container-alt-base">--}}
                        {{--<div class="arrow-container arrow-left">--}}
                            {{--<i class="fas fa-arrow-left"></i>--}}
                        {{--</div>--}}
                        {{--<div class="carousel-container-alt">--}}
                            {{--<div class="content">--}}
                                {{--<div class="quick-link flex-center">--}}
                                    {{--<i class="fas fa-cart-plus"></i>--}}
                                    {{--<h4>Shop Now</h4>--}}
                                {{--</div>--}}
                                {{--<img src="images/i1.png" alt="" />--}}
                                {{--<div class="item-desc flex-center flex-column">--}}
                                    {{--<h5>Flat custom icon</h5>--}}
                                    {{--<h6>For web and limitless purposes</h6>--}}
                                {{--</div>--}}

                            {{--</div>--}}
                            {{--<div class="content">--}}
                                {{--<div class="quick-link flex-center">--}}
                                    {{--<i class="fas fa-cart-plus"></i>--}}
                                    {{--<h4>Shop Now</h4>--}}
                                {{--</div>--}}
                                {{--<img src="images/i3.png" alt="" />--}}
                                {{--<div class="item-desc flex-center flex-column">--}}
                                    {{--<h5>Fruit icon</h5>--}}
                                    {{--<h6>For website and mobile app</h6>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="content">--}}
                                {{--<div class="quick-link flex-center">--}}
                                    {{--<i class="fas fa-cart-plus"></i>--}}
                                    {{--<h4>Shop Now</h4>--}}
                                {{--</div>--}}
                                {{--<img src="images/i4.png" alt="" />--}}
                                {{--<div class="item-desc flex-center flex-column">--}}
                                    {{--<h5>Fruit icon</h5>--}}
                                    {{--<h6>For website and mobile app</h6>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="content">--}}
                                {{--<div class="quick-link flex-center">--}}
                                    {{--<i class="fas fa-cart-plus"></i>--}}
                                    {{--<h4>Shop Now</h4>--}}
                                {{--</div>--}}
                                {{--<img src="images/i5.png" alt="" />--}}
                                {{--<div class="item-desc flex-center flex-column">--}}
                                    {{--<h5>Watch Name</h5>--}}
                                    {{--<h6>Sub desc</h6>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="arrow-container arrow-right">--}}
                            {{--<i class="fas fa-arrow-right"></i>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                {{--</div>--}}

            {{--</div>--}}
            <div id="tabs3" class="container-fluid">
                <div class="container-fluid slider" >

                    <h1> <a  style="display: flex; justify-content: center" href="http://creaticode.com/blog"></a></h1>
                    <!-- ====================================
                    Contenedor Slider
                    =======================================-->
                    <section id="slider" class="container">
                        <ul class="slider-wrapper">
                            <li class="current-slide">
                                <img src="images/w1.jpg" title="" alt="">

                                <div class="caption">
                                    <h2 class="slider-title">WHY US ?</h2>
                                    <p></p>
                                </div>
                            </li>

                            <li>
                                <img src="images/w3.jpg" title="" alt="">

                                <div class="caption">
                                    <h2 class="slider-title">WHY US ?</h2>
                                    <p>BECAUSE WE CARE FOR YOU</p>
                                </div>
                            </li>

                            <li>
                                <img src="images/w2.jpg" title="" alt="">

                                <div class="caption">
                                    <h2 class="slider-title">WHY US?</h2>
                                    <p>BECAUSE WE ARE DIFFERENT </p>
                                </div>
                            </li>

                            <li>
                                <img src="images/wx.jpg" title="" alt="">

                                <div class="caption">
                                    <h2 class="slider-title">WHY US ?</h2>
                                    <p> BECAUSE WE THINK OUT OF THE BOX.</p>
                                </div>
                            </li>
                        </ul>
                        <!-- Sombras -->
                        <div class="slider-shadow"></div>

                        <!-- Controles de Navegacion -->
                        <ul id="control-buttons" class="control-buttons"></ul>
                    </section>


                    <!-- Imagenes Copyright -->


                </div>



            </div>

        </div>
    </div>

</div>
</div>

<footer >
    <div style="display:flex;margin-top: 20px; align-content:center; justify-content:center;color:white; height:50px; background-color: #2a6280;">
        <h style="text-align: center; margin-top:20px;display: flex;justify-content: end; font-family: 'Arial Black';color: white"> WE ARE SOCIAL </h>
    </div>

    <div style="display:flex; align-content:center; justify-content:center;color:white;margin-top:0px; height:50px; background-color: #2a6280;">
        <div class="social"></div>
        <a href="#"><img src="images/fb.png" style="height: 50px;width: 50px; padding: 5px"></a>
        <a href="#"><img src="images/insta.png" style="height: 50px;width: 50px;padding: 5px"></a>
        <a href="#"><img src="images/gm.png" style="height: 50px;width: 50px;padding: 5px"></a>
        <a href="#"><img src="images/linked.png" style="height: 50px;width: 50px;padding: 5px"></a>
    </div>
    <div style="display:flex; font-family: 'Arial Black';align-content:center; justify-content:center;color:white;  background-color: #2a6280;">
        <p style="text-align: center; margin-top:20px;display: flex;justify-content: end">&copy;  SAAD  & FARAN</p>
    </div>

</footer>

<script
    src="https://code.jquery.com/jquery-3.3.1.js"
    integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
    crossorigin="anonymous"></script>
<script
    src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
    integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="0
    crossorigin="anonymous"></script>
<script type="application/javascript" src="js/scripts.js"></script>
<script type="application/javascript" src="https://threejs.org/examples/js/libs/stats.min.js"></script>

<script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
<script src="js/particles.js"></script>


</body>
</html>

