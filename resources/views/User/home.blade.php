@extends("User.admin.layouts.master")


@section("content")
    <style>
        .fc-day-number{
            color:white;
        }
        .fc-title{
            background: #f78536;
        }
        .fc-content{
            background: #f78536;
        }
        .fc-event{
            border: 1px solid #f78536;

            background-color: #f78536;
        }
        .fc-month-button{
            display:none
        }
        .fc-agendaWeek-button{
            display:none;
        }
        .fc-agendaDay-button{
            display:none
        }
        .fc-day-header{
            background:#2b4450 ;
            color:white;
        }
        .fc-center{
            color: white;
        }
        .fc-today{
            color:black;
            background-color: #fff!important;
        }
        .fc-prev-button{
            background-color:#2b4450 ;
        }
        #calendar-a1KbvBMC{
            color:#2b4450
        }
    </style>
    <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700);




        .animation {
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease;
        }


        .containers {



            /* Set the depth of the elements */
            -webkit-perspective: 800px;
            -moz-perspective: 800px;
            -o-perspective: 800px;
            perspective: 800px;
        }

        .containers:first-child {
            margin-left: 0;
        }

        .cards {



            /* Set the transition effects */
            -webkit-transition: -webkit-transform 0.4s;
            -moz-transition: -moz-transform 0.4s;
            -o-transition: -o-transform 0.4s;
            transition: transform 0.4s;
            -webkit-transform-style: preserve-3d;
            -moz-transform-style: preserve-3d;
            -o-transform-style: preserve-3d;
            transform-style: preserve-3d;
        }

        .cards.flipped {
            -webkit-transform: rotateY( 180deg );
            -moz-transform: rotateY( 180deg );
            -o-transform: rotateY( 180deg );
            transform: rotateY( 180deg );
        }

        .cards .front,
        .cards .back {
            display: block;
            height: 100%;
            width: 100%;
            line-height: 260px;
            color: white;
            text-align: center;
            font-size: 4em;
            position: absolute;
            -webkit-backface-visibility: hidden;
            -moz-backface-visibility: hidden;
            -o-backface-visibility: hidden;
            backface-visibility: hidden;

            box-shadow: 3px 5px 20px 2px rgba(0, 0, 0, 0.25);
        }

        .cards.flipped .fronts{
            display: none!important;
        }
        .cards.flipped .col-md-6{
            display: none!important;
        }

        .cards .back {
            width: 94%;
            padding-left: 3%;
            padding-right: 3%;
            font-size: 16px;
            text-align: left;
            line-height: 25px;

        }

        .formItems:first-child {
            margin-top: 20px;
        }

        .cards .back label {
            display: inline-block;
            width: 70px;
            text-align: left;
        }

        .cards .fronts {
            background: ;
        }

        .cards .back {
            background: #2b4450;
            -webkit-transform: rotateY( 180deg );
            -moz-transform: rotateY( 180deg );
            -o-transform: rotateY( 180deg );
            transform: rotateY( 180deg );
        }

        .containers:first-child .cards .fronts {
            background: #228653;
        }

        .containers:first-child .cards .back {
            background: #007539;
        }

        .cardTitles {
            font-size: 1.4em;
            line-height: 1.2em;
            margin: 0;
        }

        .contents {
            padding: 4%;
            font-weight: 100;
            text-align: left;
        }

        button.btnSend {
            display: inline-block;
            min-width: 100px;
            padding: 3px 5px;
            margin-top: 10px;
            font-weight: bold;
            text-transform: uppercase;
            text-align: center;
            color: #03446A;
            background: #fff;
            border: 0;
            border-radius: 3;
        }
    </style>
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div>
                </div>
            </div>
        </div>
        @if(count($errors) > 0 )
            @foreach($errors->all() as $error)
                <script>
                    swal("AH OH!", "{{ $error }}", "error");
                </script>
            @endforeach
        @endif

        @if(Session::has('created'))
            <script>
                swal("Great!", "{{session('created')}}", "success");
            </script>
        @endif

        @if(Session::has('updated'))
            <script>
                swal("Great!", "{{session('updated')}}", "success");
            </script>
        @endif
        @if(Session::has('test_created') )
            <script>
                swal("Great", "{{ session('created') }}", "success");
            </script>
        @endif
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-pencil-alt"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Jobs</span>
                                <span class="info-box-number">{{ $jobCount }}</span>
                            </div>
                        </div>
                    </div>


                    <div class="clearfix hidden-md-up"></div>

                <div class="modal fade" id="service-modal"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Create Booking</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            </ol>
                    </div>
                </div>
            </div>
        </section>

        @if(count($errors) > 0 )
            @foreach($errors->all() as $error)
                <script>
                    swal("AH OH!", "{{ $error }}", "error");
                </script>
            @endforeach
        @endif

        @if(Session::has('image_created'))
            <script>
                swal("Great!", "{{session('image_created')}}", "success");
            </script>
        @endif
        @if(Session::has('applied'))
            <script>
                swal("Great!", "{{session('applied')}}", "success");
            </script>
        @endif
        @if(Session::has('already'))
            <script>
                swal("HEY!", "{{session('already')}}", "error");
            </script>
        @endif

        @if(Session::has('image_updated'))
            <script>
                swal("Great!", "{{session('image_updated')}}", "success");
            </script>
        @endif


        <section class="content">
            <div class="container-fluid">
                <div class="card card-primary card-outline" style="margin-top: -399px">
                    <div class="card-header">
                        <div class="row">

                            <h2>Jobs</h2>
                            <div class="px-1"></div>

                            <a class="message-modal-r" href="#" >
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped table-responsive" id="table">
                            <thead>
                            <tr>
                                <th scope="col">SR No</th>
                                <th scope="col">Job Position</th>
                                <th scope="col">Job Description</th>
                                <th> Read More</th>
                            </tr>

                            </thead>
                            <tbody>
                        @if(isset($jobs))
                            @foreach($jobs as $job)
                                <tr class="del" data-id="{{$job->id}}">
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $job->job_position }}</td>
                                    <td>{{ str_limit(strip_tags($job->job_description),150)}}</td>
                                    <td><a class="btn btn-outline-primary" href="{{ route('specific.job', $job->id) }}">Read More</a></td>
                                </tr>
                            @endforeach
                        @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>

    </div>



    <div class="modal fade" id="image-modal"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Upload Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" onsubmit="return validateForm()" method="post"  enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="file" class="image" name="image">
                        </div>

                        <div class="form-group">
                            <input class="btn btn-primary" type="submit" name="submit" value="Create">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>





@stop
