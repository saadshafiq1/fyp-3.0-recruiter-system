@extends("User.admin.layouts.master")
{{--this is the cv form\--}}
<style>
    html,body {padding:0;margin:0;}
    .wrap {
        font:12px Arial, san-serif;
    }
    h4{
        padding-left: 4%;
        color: #4e555b;
    }
    h1.likert-header {
        padding-left:4.25%;
        margin:20px 0 0;
    }
    form .statement {
        display:block;
        font-size: 14px;
        font-weight: bold;
        padding: 30px 0 0 4.25%;
        margin-bottom:10px;
    }
    form .likert {
        list-style:none;
        width:100%;
        margin:0;
        padding:0 0 35px;
        display:block;
        border-bottom:2px solid #efefef;
    }
    form .likert:last-of-type {border-bottom:0;}
    form .likert:before {
        content: '';
        position:relative;
        top:11px;
        left:9.5%;
        display:block;
        background-color:#efefef;
        height:4px;
        width:78%;
    }
    form .likert li {
        display:inline-block;
        width:14%;
        text-align:center;
        vertical-align: top;
    }
    form .likert li input[type=radio] {
        display:block;
        position:relative;
        top:0;
        left:50%;
        margin-left:-6px;

    }
    form .likert li label {width:100%;}
    form .buttons {
        margin:30px 0;
        padding:0 4.25%;
        text-align:right
    }
    form .buttons button {
        padding: 5px 10px;
        background-color: #67ab49;
        border: 0;
        border-radius: 3px;
    }
    form .buttons .clear {background-color: #e9e9e9;}
    form .buttons .submit {background-color: #67ab49;}
    form .buttons .clear:hover {background-color: #ccc;}
    form .buttons .submit:hover {background-color: #14892c;}
</style>
@section("content")
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1></h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        @if(count($errors) > 0 )
            @foreach($errors->all() as $error)
                <script>
                    swal("AH OH!", "{{ $error }}", "error");
                </script>
            @endforeach
        @endif

        @if(Session::has('test_created') )
                <script>
                    swal("Great", "{{ session('created') }}", "success");
                </script>
        @endif

        <div class="wrap">
            <h1 class="likert-header">Personality Test</h1>
            <h4>The answers will not be provided to you but would be forwarded to the recruiter with your other information.</h4>
            <h4 style="padding-left:4%"> The following personality test is not compulsory to take but recommended.</h4>
            <h4>Total time recommended to attempt is 30 min</h4>
            <h4>Test is judged by a machine trained model.</h4>
            <h4>sTest contains total of 60 questions</h4>


            <form id="test"  role="form" method="POST" action="{{ route('give.test') }}" enctype="multipart/form-data" onsubmit=" return validateForm()">
                @csrf
                <label class="statement">You enjoy vibrant social events with lots of people.</label>
                <ul class='likert'>
                    <li>
                        <input checked type="radio" name="likert1" value="1" >
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert1" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert1" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert1" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert1" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert1" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert1" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You often spend time exploring unrealistic yet intriguing ideas.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert2" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert2" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert2" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert2" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert2" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert2" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert2" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">Your travel plans are more likely to look like a rough list of ideas than a detailed itinerary.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert3" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert3" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert3" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert3" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert3" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert3" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert3" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You often think about what you should have said in a conversation long after it has taken place.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert4" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert4" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert4" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert4" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert4" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert4" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert4" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">If your friend is sad about something, your first instinct is to support them emotionally, not try to solve their problem.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert5" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert5" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert5" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert5" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert5" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert5" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert5" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">People can rarely upset you.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert6" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert6" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert6" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert6" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert6" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert6" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert6" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You often rely on other people to be the ones to start a conversation and keep it going.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert7" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert7" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert7" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert7" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert7" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert7" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert7" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">If you have to temporarily put your plans on hold, you make sure it is your top priority to get back on track as soon as possible.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert8" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert8" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert8" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert8" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert8" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert8" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert8" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You rarely worry if you made a good impression on someone you met.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert9" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert9" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert9" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert9" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert9" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert9" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert9" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">It would be a challenge for you to spend the whole weekend all by yourself without feeling bored.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert10" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert10" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert10" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert10" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert10" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert10" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert10" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You are more of a detail-oriented than a big picture person.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert11" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert11" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert11" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert11" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert11" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert11" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert11" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You are very affectionate with people you care about.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert12" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert12" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert12" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert12" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert12" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert12" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert12" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You have a careful and methodical approach to life.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert13" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert13" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert13" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert13" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert13" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert13" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert13" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You are still bothered by the mistakes you made a long time ago.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert14" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert14" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert14" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert14" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert14" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert14" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert14" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">At parties and similar events you can mostly be found farther away from the action.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert15" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert15" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert15" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert15" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert15" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert15" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert15" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You often find it difficult to relate to people who let their emotions guide them.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert16" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert16" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert16" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert16" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert16" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert16" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert16" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">When looking for a movie to watch, you can spend ages browsing the catalog.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert17" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert17" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert17" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert17" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert17" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert17" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert17" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You can stay calm under a lot of pressure.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert18" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert18" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert18" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert18" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert18" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert18" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert18" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">When in a group of people you do not know, you have no problem jumping right into their conversation.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert19" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert19" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert19" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert19" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert19" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert19" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert19" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">When you sleep, your dreams tend to be bizarre and fantastical.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert20" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert20" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert20" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert20" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert20" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert20" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert20" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">In your opinion, it is sometimes OK to step on others to get ahead in life.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert21" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert21" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert21" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert21" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert21" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert21" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert21" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You are dedicated and focused on your goals, only rarely getting sidetracked.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert22" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert22" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert22" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert22" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert22" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert22" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert22" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">If you make a mistake, you tend to start doubting yourself, your abilities, or your knowledge.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert23" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert23" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert23" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert23" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert23" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert23" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert23" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">When at a social event, you rarely try to introduce yourself to new people and mostly talk to the ones you already know.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert24" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert24" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert24" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert24" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert24" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert24" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert24" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You usually lose interest in a discussion when it gets philosophical.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert25" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert25" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert25" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert25" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert25" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert25" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert25" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You would never let yourself cry in front of others.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert26" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert26" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert26" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert26" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert26" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert26" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert26" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You feel more drawn to places with a bustling and busy atmosphere than to more quiet and intimate ones.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert27" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert27" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert27" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert27" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert27" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert27" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert27" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You like discussing different views and theories on what the world could look like in the future.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert28" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert28" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert28" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert28" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert28" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert28" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert28" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">When it comes to making life-changing choices, you mostly listen to your heart rather than your head.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert29" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert29" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert29" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert29" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert29" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert29" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert29" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You cannot imagine yourself dedicating your life to the study of something that you cannot see, touch, or experience.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert30" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert30" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert30" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert30" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert30" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert30" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert30" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You usually prefer to get your revenge rather than forgive.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert31" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert31" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert31" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert31" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert31" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert31" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert31" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You often make decisions on a whim.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert32" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert32" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert32" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert32" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert32" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert32" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert32" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">The time you spend by yourself often ends up being more interesting and satisfying than the time you spend with other people.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert33" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert33" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert33" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert33" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert33" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert33" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert33" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You often put special effort into interpreting the real meaning or the message of a song or a movie.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert34" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert34" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert34" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert34" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert34" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert34" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert34" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You always know exactly what you want.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert35" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert35" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert35" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert35" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert35" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert35" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert35" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You rarely think back on the choices you made and wonder what you could have done differently.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert36" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert36" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert36" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert36" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert36" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert36" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert36" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">When in a public place, you usually stick to quieter and less crowded areas.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert37" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert37" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert37" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert37" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert37" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert37" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert37" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You tend to focus on present realities rather than future possibilities.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert38" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert38" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert38" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert38" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert38" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert38" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert38" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You often have a hard time understanding other people’s feelings.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert39" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert39" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert39" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert39" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert39" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert39" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert39" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">When starting to work on a project, you prefer to make as many decisions upfront as possible.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert40" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert40" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert40" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert40" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert40" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert40" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert40" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">When you know someone thinks highly of you, you also wonder how long it will be until they become disappointed in you.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert41" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert41" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert41" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert41" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert41" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert41" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert41" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You feel comfortable just walking up to someone you find interesting and striking up a conversation.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert42" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert42" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert42" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert42" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert42" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert42" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert42" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You often drift away into daydreaming about various ideas or scenarios.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert43" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert43" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert43" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert43" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert43" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert43" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert43" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You look after yourself first, and others come in second.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert44" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert44" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert44" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert44" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert44" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert44" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert44" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">Even when you have planned a particular daily routine, you usually just end up doing what you feel like at any given moment.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert45" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert45" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert45" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert45" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert45" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert45" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert45" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">Your mood can change very quickly.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert46" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert46" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert46" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert46" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert46" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert46" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert46" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You often contemplate the reasons for human existence or the meaning of life.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert47" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert47" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert47" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert47" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert47" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert47" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert47" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You often talk about your own feelings and emotions.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert48" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert48" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert48" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert48" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert48" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert48" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert48" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You have got detailed education or career development plans stretching several years into the future.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert49" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert49" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert49" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert49" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert49" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert49" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert49" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You rarely dwell on your regrets.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert50" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert50" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert50" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert50" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert50" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert50" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert50" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">Spending time in a dynamic atmosphere with lots of people around quickly makes you feel drained and in need of a getaway.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert51" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert51" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert51" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert51" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert51" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert51" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert51" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You see yourself as more of a realist than a visionary.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert52" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert52" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert52" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert52" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert52" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert52" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert52" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You find it easy to empathize with a person who has gone through something you never have.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert53" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert53" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert53" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert53" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert53" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert53" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert53" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">Your personal work style is closer to spontaneous bursts of energy than to organized and consistent efforts.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert54" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert54" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert54" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert54" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert54" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert54" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert54" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">Your emotions control you more than you control them.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert55" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert55" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert55" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert55" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert55" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert55" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert55" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">After a long and exhausting week, a fun party is just what you need.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert56" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert56" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert56" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert56" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert56" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert56" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert56" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You frequently find yourself wondering how technological advancement could change everyday life.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert57" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert57" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert57" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert57" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert57" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert57" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert57" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You always consider how your actions might affect other people before doing something.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert58" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert58" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert58" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert58" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert58" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert58" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert58" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You still honor the commitments you have made even if you have a change of heart.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert59" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert59" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert59" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert59" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert59" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert59" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert59" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <label class="statement">You rarely feel insecure.</label>
                <ul class='likert'>
                    <li>
                        <input type="radio" name="likert60" value="1">
                        <label>Very Strongly agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert60" value="2">
                        <label>Strong Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert60" value="3">
                        <label>Agree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert60" value="4">
                        <label>neutral</label>
                    </li>
                    <li>
                        <input type="radio" name="likert60" value="5">
                        <label>disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert60" value="6">
                        <label>Strong disagree</label>
                    </li>
                    <li>
                        <input type="radio" name="likert60" value="7">
                        <label>Very Strong disagree</label>
                    </li>
                </ul>
                <div class="buttons">
                    <button  class="btn btn-info" class="submit">Submit</button>
                </div>
            </form>
        </div>
        </section>
    </div>
@stop

@section("extra_js")
    <script>
        $(document).ready(function(){
            var length = $(".likert").length
            console.log(length)
            for(var count=0; count<length; count++){
                $("ul.likert:nth-of-type("+(count+1)+") li:first-child input").attr("checked", "checked")
            }
        })
    </script>

@stop
