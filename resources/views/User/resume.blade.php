@extends("User.admin.layouts.master")


@section("content")
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1></h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Icons</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        @if(count($errors) > 0 )
            @foreach($errors->all() as $error)
                <script>
                    swal("AH OH!", "{{ $error }}", "error");
                </script>
            @endforeach
        @endif


        <section class="content">
            <div class="container-fluid">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Resume</h3>
                    </div>
                    <div class="card-body">
                        <form id="create_form"  role="form" method="POST" action="{{ route('data.store') }}" enctype="multipart/form-data" onsubmit=" return validateForm()">
                            {{ csrf_field() }}
                            <div class="box-body" style="height: 100%;">
                                <div class="form-group">
                                    <label for="title">Experience</label>
                                    <input name="experience" type="number" class="form-control" id="name" value="{{ old('name') }}" placeholder="Job Experience in months">
                                    <span class="error" id="title_error" aria-live="polite"></span>
                                </div>
                                <div class="form-group">
                                    <label for="title">Work Email</label>
                                    <input name="work_email" type="email" class="form-control" id="name" value="{{ old('work_email') }}" placeholder="abc@mail.com">
                                    <span class="error" id="title_error" aria-live="polite"></span>
                                </div>
                                <div class="form-group">
                                    <label for="title">Contact</label>
                                    <input name="mobile_number" type="text" class="form-control" id="name" value="{{ old('mobile_number') }}" placeholder="XXXX-XXXXXXX">
                                    <span class="error" id="title_error" aria-live="polite"></span>
                                </div>
                                <div class="form-group">
                                    <label for="title">Description</label>
                                    <textarea name="description" class="form-control" id="name" value="{{ old('description') }}" placeholder="Kindly write a brief description about your :
Educational background , Achievement(s) , and anything you want the recruiter to know about ."></textarea>
                                    <span class="error" id="title_error" aria-live="polite"></span>
                                </div>
                                <div class="form-group">
                                    <label for="title">Expected Salary</label>
                                    <input name="salary" type="text" class="form-control" id="link" value="{{ old('link') }}" placeholder="Enter Salary ">
                                    <span class="error" id="title_error" aria-live="polite"></span>
                                </div>
                                <div class="form-group">
                                    <label for="title">Main Skill(s)</label>
                                    <input name="skills" type="text" class="form-control" id="link"@if(isset($user->skills)) value="{{ $user->skills }}">@endif
                                    <span class="error" id="title_error" aria-live="polite"></span>
                                </div>
                                <div class="form-group" style="padding-top: 10px;">
                                    <label for="description">Gender:</label>
                                    <input name="gender" type="checkbox" value="0">Male
                                    <input name="gender" type="checkbox" value="1">Female
                                    <span class="error" id="description_error" aria-live="polite"></span>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
