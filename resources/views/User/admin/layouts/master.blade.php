<!DOCTYPE html>
<html lang="en">

@include('User.admin.layouts.head')

<body class="hold-transition sidebar-mini">

<div class="wrapper">

    @include('User.admin.layouts.header')

    @include('User.admin.layouts.sidebar')

    @yield('content')

    @include('User.admin.layouts.footer')

</div>

</body>

@include('User.admin.layouts.scripts')

@yield("extra_js")
</html>











