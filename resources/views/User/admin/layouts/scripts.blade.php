<script src="{{URL::to('admin_ui/js/jquery.min.js')}}"></script>
<script src="{{ URL::to('admin_ui/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ URL::to('admin_ui/js/adminlte.js') }}"></script>
<script src="{{ URL::to('admin_ui/js/jquery.sparkline.min.js') }}"></script>
<script src="{{ URL::to('admin_ui/js/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ URL::to('admin_ui/js/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ URL::to('admin_ui/js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ URL::to('admin_ui/js/Chart.min.js') }}"></script>
<script src="{{ URL::to('admin_ui/js/dashboard2.js') }}"></script>
<script src="{{ URL::to('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ URL::to('https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ URL::to('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-iconpicker/1.10.0/js/bootstrap-iconpicker.bundle.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>





@yield('extra_scripts')

