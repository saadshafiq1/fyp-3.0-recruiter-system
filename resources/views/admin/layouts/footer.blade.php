<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 3.0
    </div>
    <strong>Copyright &copy; 2018-2019 <a href="#">SAAD & FARAN FYP</a>.</strong> All rights
    reserved.
</footer>
