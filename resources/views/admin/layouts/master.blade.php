<!DOCTYPE html>
<html lang="en">

@include('admin.layouts.head')

<body class="hold-transition sidebar-mini">

<div class="wrapper">

    @include('admin.layouts.header')

    @include('admin.layouts.sidebar')

    @yield('content')

    @include('admin.layouts.footer')

</div>

</body>

@include('admin.layouts.scripts')


</html>











