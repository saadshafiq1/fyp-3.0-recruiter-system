<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>SBA</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ URL::to('admin_ui/css/font-awesome.min.css') }}">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-iconpicker/1.10.0/css/bootstrap-iconpicker.css"/>

    <link rel="stylesheet" href="{{ URL::to('admin_ui/css/adminlte.min.css') }}">

    <link rel="stylesheet" href="{{ URL::to('admin_ui/summernote/summernote-bs4.css') }}">

    <link rel="stylesheet" href="{{ URL::to('https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css') }}">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.css" />


    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="{{ URL::to('admin_ui/css/style.css') }}">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">



    <link rel="stylesheet" href="{{URL::to('admin_ui/plugins/fullcalendar/fullcalendar.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('admin_ui//plugins/fullcalendar/fullcalendar.print.css')}}" media="print">
    @yield('extra_css')
    <style>
        .fc-title{
            color: white;
        }
        .fc-content{
            cursor: pointer;
        }
    </style>
</head>
