<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="{{ route('admin.dashboard') }}" class="brand-link">
        <img src="{{ URL::to('admin_ui/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Recruiter</span>
    </a>
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">

            </div>
            <div class="info">
                <a href="{{ route('recruiter.jobs') }}" class="d-block">{{Auth::guard('admin')->user()->first_name}}</a>
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-item has-treeview">
                    <a href="{{ route('recruiter.jobs') }}" class="nav-link {{ Request::path() == 'admin'?'active':  ''}} ">
                        <i class="fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>

                <li class="nav-item has-treeview">
                    <a href="{{ route('admin.job.create') }}" class="nav-link {{ Request::path() == 'admin'?'active':  ''}} ">
                        <i class="fas fa-tachometer-alt"></i>
                        <p>
                            Create Job
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ route('recruiter.jobs') }}" class="nav-link {{ Request::path() == 'admin'?'active':  ''}} ">
                        <i class="fas fa-tachometer-alt"></i>
                        <p>
                            User's Request For Job
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
