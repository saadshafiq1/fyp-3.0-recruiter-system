import json
import numpy as np
import pymysql
from datetime import datetime
from sklearn.model_selection import train_test_split
from sklearn.svm import *
from sklearn.neighbors import KNeighborsClassifier
from sklearn import linear_model
from sklearn import tree
import pickle
from sklearn.externals import joblib

now = datetime.now()
currentDate = now.strftime("%Y-%m-%d %H:%M:%S")

filename = "finalized_model.pkl"

labels = ["More EXTRAVERTED than INTROVERTED", "More INTUITIVE than OBSERVANT", "Relies more on THINKING than FEELINGS", "This person has more of a JUDGING personallity than PROSPECTING one ", "Is ASSERTIVE than TURBULENT  "]

conn =pymysql.connect(host="127.0.0.1",user="root",password="",db="recruitingsystem")
a= conn.cursor()
sql ='SELECT * FROM `tests` ORDER BY `tests`.`id` DESC'
a.execute(sql)
countrow =a.execute(sql)
data = a.fetchone()

user_id = data[1]
test_id = data[0]
data = list(data)[2:-2]


x = np.mat(data) # this list will have the data for which the prediction has to be made

loaded_model = joblib.load(open(filename, 'rb'))
result = []
for i in loaded_model:
    result.append(i.predict(x))

personalityArray = []

# Format the SQL Query
sqlInsertString ="VALUES"
for i in range (0, len(result)):
    if result[i] == 1:
        sqlInsertString += " (NULL, '%s', '%s', '%s', '%s', '%s')," % (user_id, test_id, labels[i], currentDate, currentDate)
        personalityArray.append(labels[i])
        # print(labels[i])

sqlInsertString = sqlInsertString[0:-1]

a= conn.cursor()
sql =" INSERT INTO `user_personalities` (`id`, `user_id`, `test_id`, `name`, `created_at`, `updated_at`) %s ;" % (sqlInsertString)
a.execute(sql)
conn.commit()

# Return output for laravel to use json_decode and use the results in realtime.
print(json.dumps(personalityArray))
